Imports and commits an svn repository to a git repository, keeping the commit history intact

Uses subgit version 3.0.0 EAP (tested in 3053), because 2.x does not support the 'notrunk' option

Use 'import-trunk.bat' for repositories with the standard trunk/branches/tags layout
Use 'import-notrunk.bat' for repositories that are just code

Only tested with bitbucket git URLs, but will most likely work with others too

Usage:

import-trunk.bat url-to-svn url-to-git

For example:

import-trunk.bat https://server:443/svn/repository/ http://bitbucket.org/lambik/repository.git

Setup:

* unzip subgit somewhere
* open the bat file with a text editor
* change the line that CD's to the subgit bin directory
* create your authors.txt file using the layout in the demo file