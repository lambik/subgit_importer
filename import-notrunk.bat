@echo off

REM for svn repositories without trunk/branches/tags structure

REM call with: import-notrunk.bat https://svn.url/repo https://bitbucket/repo.git
REM make sure folder "src" does not exist
REM make sure authors.txt exists in this folder

cls

REM set up subgit folder
cd c:\temp\bin

cmd /c subgit configure %1 src

copy /y "%~dp0authors.txt" src\subgit\

git config -f src/subgit/config --unset svn.trunk
git config -f src/subgit/config --unset svn.branches
git config -f src/subgit/config --unset svn.tags
git config -f src/subgit/config --unset svn.shelves

cmd /c subgit import src

cd src

git remote add origin %2

git push -u origin --all

cd ..

rmdir /S /Q src
